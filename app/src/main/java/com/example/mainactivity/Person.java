package com.example.mainactivity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName="person_table")
public class Person {

    @PrimaryKey(autoGenerate = true)
    private int id;

    // set the properites of each column
    @NonNull
    @ColumnInfo(name = "firstname")
    private String firstName;

    @NonNull
    @ColumnInfo(name = "lastname")
    private String lastName;


    @ColumnInfo(name = "dept")
    private String department;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "salary")
    private double salary;

    public Person(@NonNull String firstName, @NonNull String lastName, String department, String gender, double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.gender = gender;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    // getter and setter for id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
