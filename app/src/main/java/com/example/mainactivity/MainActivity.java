package com.example.mainactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<Person> employees;
    PersonListViewAdapter adapter;

    final String TAG = "EMPLOYEE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // get employees from database
        List<Person> users= StartScreenActivity.db.personDoa().getAllPersons();

        // Put the employees into an ArrayList
        // We will use the ArrayList with the PersonListAdapter
        employees  = (ArrayList<Person>) users;

        adapter  = new PersonListViewAdapter(this,R.layout.list_layout,employees);
        //  ArrayAdapter<String> adapter = new ArrayAdapter<String> (this, android.R.layout.simple_list_item_1,employeeNames);
        final ListView mainView = (ListView) findViewById(R.id.mainListview);

        mainView.setAdapter(adapter);

        AdapterView.OnItemClickListener abc =
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // @TODO: Write code to handle item click

                        // 1. OPTIONAL: Show position of row the person clicked on
                        // "position" variable comes from function arguments
                        Log.d("JENELLE","You clicked on row: " + position);

                        // 2. Get the object at that row
                        Person p = (Person) mainView.getItemAtPosition(position);


                        // 3. Output the person object data to the console
                        Log.d(TAG, "-----------------");
                        Log.d(TAG, "Person info: ");
                        Log.d(TAG, "Name: " + p.getFirstName());
                        Log.d(TAG, "Salary: " + p.getSalary());

                        // 4. OPTIONAL: Show person data in a toast
                        String message = "Row " + position + ":" + p.getFirstName() + "," + p.getSalary();
                        Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                        t.show();
                    }
                };

        mainView.setOnItemClickListener(abc);
    }

    public void addPersonPressed(View view) {

        // 1. Create a new person
        Person p1 = new Person("Monica","Summers","Sales","M",80231);

        // 2. LOGIC: Add person to the ARRAYLIST
        employees.add(p1);

        // 3. UI: Refresh the ListView
        // This function:
        // - tells Android that your data source (array) has changed
        // - Therefore, please reload the Listview with the updated data
        // For this function to work, you need adapter & listview variable to be global
        adapter.notifyDataSetChanged();
    }
}
