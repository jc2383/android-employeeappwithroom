package com.example.mainactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class StartScreenActivity extends AppCompatActivity {

    // Database connection variable
    public static PersonDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        // 1. setup the database connection
        db = Room.databaseBuilder(getApplicationContext(),
                PersonDatabase.class,"persons").allowMainThreadQueries().build();
    }

    public void addEmployeePressed(View view) {
        EditText etFirstName = (EditText) findViewById(R.id.etEmployeeFirstName);
        EditText etLastName = (EditText) findViewById(R.id.etEmployeeLastName);
        EditText etDept = (EditText) findViewById(R.id.etDept);
        EditText etSalary = (EditText) findViewById(R.id.etSalary);
        EditText etGender = (EditText) findViewById(R.id.etGender);

        String fname = etFirstName.getText().toString();
        String lname = etLastName.getText().toString();
        String dept = etDept.getText().toString();
        String gender = etGender.getText().toString();
        double salary = Double.valueOf(etSalary.getText().toString());

        Person person = new Person(fname, lname, dept, gender, salary);

        db.personDoa().insert(person);

        Toast t = Toast.makeText(getApplicationContext(), "Person inserted", Toast.LENGTH_SHORT);
        t.show();

    }


    public void showEmployeesPressed(View view) {
        Intent i = new Intent(StartScreenActivity.this, MainActivity.class);
        startActivity(i);
    }





}
