package com.example.mainactivity;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PersonDao {

    // OnConflictStrategy.IGNORE = if the insert fails, then the app will not insert the row
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Person person);

    // setup delete query
    @Query("DELETE FROM person_table")
    void deleteAll();

    // setup SELECT * query
    @Query("SELECT * from person_table")
    List<Person> getAllPersons();

}
